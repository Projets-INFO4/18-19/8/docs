# Maquette de notre application mobile

Avant de commencer le développement de notre application mobile, nous avons créé
une maquette afin d'avoir une idée du rendu souhaité. Notre maquette est composée
de deux versions. Au moment du développement, il sera nécessaire de choisir une
des deux versions. Les deux versions de la maquette présentent les mêmes fonctionnalitées.

## Version 1

Dans cette version, chaque page de l'application contient une fonctionnalitée. 
La maquette est donc composée de différentes pages.

### Page principale

La page principale de notre application sera composée d'une liste. Chaque élément
correspondra à une cage qui aura un nom et un état (ON ou OFF), ici on peut voir
la cage "cage 2" qui est ON par exemple. Plusieurs actions sont possible sur cette 
page : 
- il est possible de cliquersur "ALERTES" en haut de l'application afin de faire apparaitre la liste des cages
qui ont été fermées par la présence d'une marmotte.
- il est possible cliquer sur cage pour aller à la page permettant de la mofifier
- Le bouton "+" en bas a droite permet d'ouvrir une page permettant d'ajouter une cage 
sur l'application

<br>
![images de la page principale version1](pictures/maquette/Cages_principales_v1.png)

### Cages en alertes

Cette page contient une liste similaire à la page principale sauf qu'elle contient
seulement les cages qui ont été fermées par le passage d'une marmotte. Ce sont 
donc les cages qui doivent vite être récupérées afin de préserver la santé de 
l'animal. En cliquant sur le bouton "Je suis enroute", le chercheur indique qu'il 
a bien reçu l'information et qu'il est en direction du lieu de la cage.

<br>
![images de la page des cages en alerte](pictures/maquette/Alertes_v1.png)

### Modification du nom ou de l'état d'une cage

Sur cette page, l'utilisateur peut changer le nom d'une cage et l'éteindre/allumer.
Si la cage est éteinte, l'application ne prendra pas en compte la réception d'un
signal de fermeture indiquant la prise d'une marmotte. L'utilisateur peut ensuite valider
ou annuler les modifications avec les boutons en bas de la page.

<br>
![images de la page permettant de modifier une cage](pictures/maquette/modification_v1.png)

### Changement de l'état d'une cage

Cette page s'ouvre lorsque l'utilisateur éteint une cage. En effet, cette action 
ne doit pas être pris à la légére. Puisque si la cage est indiqué comme éteinte,
l'application ne recevra pas les alertes à la fermture d'une cage provoquée par 
une marmotte.

<br>
![images de la page au changement de l'état d'une cage](pictures/maquette/ON_OFF_v1.png)

## version 2

Dans cette version, toutes les fonctionnalités sont regroupées sur une même page.

### Page principale

Cette page présente une liste des cages. Chaque item de la liste correspond à une
cage qui a un nom et un état affiché en-dessous de son nom. Il est également possible 
d'éteindre/allumer une cage en changeant l'état du "switch button" à droite de chaque 
item. Un bouton "+" flottant en bas à droite de la page permet d'ajouter une cage 
à la liste.

<br>
![images de la page principale contenant la liste des cages](pictures/maquette/Cages_v2.png)

### Cages en alerte

Cette page contient une liste similaire à la page principale sauf qu'elle contient
seulement les cages qui ont été fermées par le passage d'une marmotte. Son design
est le même que celui de la version1.

<br>
![images de la page des cages en alerte](pictures/maquette/Alertes_v1.png)

### Modification du nom d'une cage

Si l'utilisateur souhaite modifier le nom d'une cage, il lui suffit de cliquer 
sur le nom de cette dernière sur la page principale de l'application. Aprés le
clique, le clavier va s'afficher en bas de la page pour permettre à l'utilisateur 
de modifier le nom de la cage. A coté du nom, le "switch button" sera remplacé par
2 boutons pour valider ou annuler la modification.

<br>
![images de la page permettant de modifier le nom d'une cage](pictures/maquette/Modification_v2.png)

### Changement de l'état d'une cage

Cette page s'ouvre lorsque l'utilisateur éteint une cage. En effet, cette action 
ne doit pas être pris à la légére. Puisque si la cage est indiqué comme éteinte,
l'application ne recevra pas les alertes à la fermture d'une cage provoquée par 
une marmotte.

<br>
![images de la page au changement de l'état d'une cage](pictures/maquette/ON_OFF_v2.png)