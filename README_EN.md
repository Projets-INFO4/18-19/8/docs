# Application mobile Ionic/NodeRed pour Cages à marmotte connectées en LoRaWAN

## Objectif

__Context__: 
IoT represent the extension of the internet to objects and locations. The connected object is uniquely identified by the network like a computer would be. It possess the necessary intelligence to generate data or collect it from it nearby environment, and transfer it automatically on the network. On the other side of this network, a platform regroup the information collected and can then generate more data with added value, based on the exploitation of these informations over time. IoT is used in a variety of domains like the medical industry, sport, connected cities, manufactures, agriculture, risk management, smart buildings..). The Internet of Things is considered as the third evolution.

__The project__:

The alpine station Joseph Fourier, part of the UGA and the CNRS, deploy its activities at the Lautaret pass, and on the campus of Grenoble. Its mission concerns the development of research platforms, the maintenance of botanic collections, the formations of students and popularising science to the public. This station already showed interest to various IoT projects not available to the market, like the connected marmot cage allowing the biologist to intervene and get the prelevments quickly without harming the animal. The surveillance device is being developped by students from the IUT1 of Grenoble.  
The project's goal is develop a monitoring application for a set of cages.

## Technological constraints

* The application must be developped using Ionic/Cordova
* The application must be able to work without any 3G/4G

## Report of the first meeting (4 february)
The marmot capture system use a lot a cages. Each cage is identified by a name and is situated at a strategic and predefined location. It is possible to recognize a cage by identifying it with the ID sent from the sensor on the cage using LoRa. When the cage is activated, it will send messages continuously until the server respond with an acknowledgment message, and a biologist confirm the reception of the message. This confirmation can also be done using the application, where a warning message will be displayed. We can imagine a map showing which cage got triggered. At night, the biologists close all the cages to avoid trapping a marmot. Also, we must provide a solution to add and remove cages from the system.

## Logbook

### Week 1 (29 january - 3 february)

We spent this class initializing the project on gitlab, adding a Readme.md file and some basic files for the creation of a Ionic application. We also followed the given lesson on LoRaWAN to start the project in the best conditions possible. The short term objective is to identify the functional and non-fonctional requirements of the biologist.

### Week 2 (4 february - 10 february)

We spent this class meeting Mr Delbart and Mrs Araujo to get a feel of what the application and the server should do, followed by a brief explication of the tools that are going to be used developping this application.

### Week 3 (11 february - 18 february)

We spent all of week 3 trying to configure a server using Microsoft Azure, without success. In the meantime, we finished the configuration of our sodaq Explorer, so we can control what payload to send and when. We then have a stable way to send and check our messages on the LoRaServer.

### Week 4 (18 february - 25 february)

After spending way too much time trying to find a cloud solution for our server, we had to react. We decided to spend more time developping the server itself on our machines, rather than finding where to host it. We used a *NodeRed* via the **DockerFile** given by the tutorial on the website [CampusIOT](https://github.com/CampusIoT/tutorial/tree/master/nodered).
<br>
<br>
We also started to write down the tutorial to generate and execute our docker image. By providing our project on a docker image, we allow anyone with any machine to run it easily.

### Week 5 and 6 (25 february - 11 march)

These weeks, we continued developping a working draft of the project using nodered by creating a *Flow* connecting to [LoraServer](https://lora.campusiot.imag.fr/#/organizations/4/applications).
and getting back the informations sent by the *Siconia* provided by *M. Donsez*.
This information translate (for now) to the temperature of the room.  
Our first program just received this temperature, and sent it by SMS to someone.
<br>
![flow_basique](pictures/s6_flow.png)
<br><br>

This is the general idea of our project. The two sensors on the cage will send informations looking like this :
```json
{
    "cage_id" : 43,
    "status" : 1,
    "time" : "15:41:32"
}
```
We then have to prepare an appropriate answer, via a simple nodered fonction, which in an SMS could look like  :
<br>
`[Alert] A marmot entered in the cage n°43 at 15h 41m 32s, you have 30 min !`