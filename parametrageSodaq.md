Dans le cadre de notre projet, nous avions besoin d'un moyen de tester la reception de trames en LoRa. Afin de recevoir ces paquets de manière controlée et stable, nous avions besoin d'un moyen d'envoyer des données en LoRa à la demande en ayant le moyen de controler le contenu de ces trames.

## Parametrage de la carte SODAQ :

Avant de pouvoir utiliser la carte Sodaq explorer, plusieurs étapes sont nécessaires :
- Installer et parametrer Arduino IDE sur son ordinateur, et ajouter toutes les packages nécessaire à l'utilisation de SODAQ
- Développer son code en c, capable de gérer un bouton, et envoyant la trame souhaitée lors de l'appuie de ce bouton
- Réussir à uploader sur la carte SODAQ le code voulu
- Vérifier sur le LoRaserver que les trames soient bien reçues.


Le code est disponible à cette adresse :

https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/18-19/8/serveur-node-red-cage-marmotte/blob/master/Sodaq%20Explorer/sodaq_code/sodaq_code.ino


Il est utile de savoir qu'il est possible de débuguer le code du SODAQ depuis son ordinateur à l'aide du SerialUSB. (System.out visible depuis son ordinateur durant l'execution)

Petits plus :
- La led bleu sur la carte s'allume en bleu quand la carte est en attente et prête à envoyer un paquet (on peut cliquer), elle est donc éteinte dans le reste des cas.
- Il est possible de surveiller le fil d'execution de ce code en laissant le sodaq connecté a un ordinateur et en utilisant le "Serial Monitor" d'arduino IDE.