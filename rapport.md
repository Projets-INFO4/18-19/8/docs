# Application mobile Ionic/NodeRed pour Cages à marmotte connectées en LoRaWAN
- [Application mobile Ionic/NodeRed pour Cages à marmotte connectées en LoRaWAN](#application-mobile-ionicnodered-pour-cages-%C3%A0-marmotte-connect%C3%A9es-en-lorawan)
  - [Introduction](#introduction)
  - [Conception](#conception)
    - [Réception des messages](#r%C3%A9ception-des-messages)
    - [Maquette](#maquette)
      - [Version 1](#version-1)
        - [Page principale](#page-principale)
        - [Cages en alertes](#cages-en-alertes)
        - [Modification du nom ou de l'état d'une cage](#modification-du-nom-ou-de-l%C3%A9tat-dune-cage)
        - [Changement de l'état d'une cage](#changement-de-l%C3%A9tat-dune-cage)
      - [version 2](#version-2)
        - [Page principale](#page-principale-1)
        - [Cages en alerte](#cages-en-alerte)
        - [Modification du nom d'une cage](#modification-du-nom-dune-cage)
        - [Changement de l'état d'une cage](#changement-de-l%C3%A9tat-dune-cage-1)
    - [Choix techniques](#choix-techniques)
  - [Réalisation](#r%C3%A9alisation)

## Introduction 

**Equipe de développement** :
- Baptiste Bouvier (Chef de Projet)
- Damien Wykland
- Ancelin Serre

**Contexte** : 
<br>
L’Internet des objets (IoD, en anglais Internet of things, IoT) représente l’extension d’Internet à des choses et à des lieux du
monde physique. L’objet connecté à Internet est identifié par le réseau de manière
unique comme l’est un ordinateur relié à Internet. L’objet connecté embarque 
l’intelligence nécessaire pour générer des données (paramètres de fonctionnement,
données d’usage, mesures physiques de son environnement, etc.), en capter de son
environnement et les transférer automatiquement sur le réseau. A l’autre bout,
une plateforme informatique centralise et traite les données collectées et gère
les services à valeur ajoutée créés de l’exploitation des historiques des 
mesures collectées au cours du temps. 
<br>
<br>
Les domaines d’application de l’Internet
des objets couvrent des pans entiers d’activités (e-santé, e-inclusion, sport
connecté, ville connectée, usine du futur, agriculture de précision, gestion des
risques naturelles et industrielles, smart office, smart building, logistique,
déplacement doux, etc.). L’internet des objets est considéré comme la troisième
évolution.

**Le projet** :
<br>
La Station alpine *Joseph Fourier* est une unité mixte de services de l'université
Grenoble Alpes et du CNRS. Elle déploie ses activités au col du Lautaret 
(Jardin botanique alpin, Chalet-laboratoire et Galerie de l'Alpe) et sur le 
campus de Grenoble (arboretum Robert Ruffier-Lanche et serres en cours de 
construction).
<br>
<br>
Ses missions concernent le développement de plateformes de recherche, 
l’entretien de collections botaniques, la formation des étudiants et la 
vulgarisation scientifique auprès du grand public.
La Station Alpine Joseph Fourier du Col du Lautaret a déjà fait part de son 
intérêt pour plusieurs objets IoT non disponibles dans le commerce, 
comme une cage à marmotte connectée permettant au biologiste d’intervenir 
rapidement afin de faire des relevés biologiques et de relâcher l’animal 
qui meurt s’il reste trop longtemps dans la cage.
<br>
<br>
L'objet de surveillance des cages est en cours de réalisation par un groupe 
d'étudiants de l'IUT1 Grenoble (R&T et GMP). Le projet consistera à développer 
l'application de monitoring d'une ensemble de cages. Cette application est 
destiné aux biologistes qui sont munis de smartphones (Android ou iOs).
Ce projet est réalisé avec un groupe d'étudiants de l'IUT1 Grenoble (R&T et GMP).

**Notre mission** :
<br>
Nous intervenons dans le cadre de ce projet pour concevoir et développer la solution informatique. Cette dernière va se traduire par une application mobile permettant aux scientifiques, chargés d'effectuer des prélèvements sur les marmottes, d'être tenus informés si une marmotte est prise au piège dans une cage. 
Cette information est cruciale pour les scientifiques du site du Col du Lautaret. En effet, une marmotte ne peut rester plus de 30 minutes prise au piège dans la cage sans quoi elle risque de 
mourir de stress et cela n'est bien évidemment pas souhaitable.
<br>
<br>
On doit également s'assurer que l'information liée à la cage arrive jusqu'au scientifique même en l'absence de connexion internet. Les scientifiques travaillant essentiellement en zone montagneuse, le réseau 3G/4G n'est pas souvent disponible et on doit ainsi souvent se contenter d'un réseau 2G (le réseau minimum traité dans ce projet). Cela implique donc l'envoi de l'information via *SMS* aux scientifiques pour palier le manque de connexion internet.
<br>
<br>
Enfin, l'application mobile devrait permettre aux scientifiques d'avoir un visuel pratique de l'ensemble des cages utilisées avec par exemple : des informations comme la géolocalisation (avec latitude, longitude), un statut indiquant si la cage est active ou non ainsi qu'un nom identifiant la cage. Il serait aussi possible de modifier ces informations à souhait ainsi que "d'ajouter" de nouvelles cages informatiquement parlant.

## Conception

### Réception des messages 

Tout d'abord, on sait que chaque cage sera équipée de deux capteurs permettant de savoir s'il y a présence de marmotte ou non. Ces capteurs seront équipés d'émetteurs/récepteurs *LoRa* et communiqueront vers le [*LoRaServer*](https://lora.campusiot.imag.fr/). Dans ce cadre là, nous devrons ainsi connecter notre solution vers le *LoRaServer* afin de récupérer et traiter les messages envoyés par les capteurs. 
<br>
Chaque message reçu et interprêté entrainera une mise à jour de la base de données ainsi que l'envoi d'un *SMS* si le message indique la présence d'une marmotte dans une cage.
<br>
Une fois le message traité et la base de donnée mise à jour, l'utilisateur devrait pouvoir effectuer les actions résumées sur le diagramme de cas d'utilisation ci-dessous :

<div style="text-align:center"><img src ="pictures/Conception/case_diagram.jpg" />
<br>Diagramme de cas d'utilisation 
</div>

### Maquette

Avant de commencer le développement de notre application mobile, nous avons créé
une maquette afin d'avoir une idée du rendu souhaité. Notre maquette est composée
de deux versions. Au moment du développement, il sera nécessaire de choisir une
des deux versions. Les deux versions de la maquette présentent les mêmes fonctionnalitées.

#### Version 1

Dans cette version, chaque page de l'application contient une fonctionnalitée. 
La maquette est donc composée de différentes pages.

##### Page principale

La page principale de notre application sera composée d'une liste. Chaque élément
correspondra à une cage qui aura un nom et un état (ON ou OFF), ici on peut voir
la cage "cage 2" qui est ON par exemple. Plusieurs actions sont possible sur cette 
page : 
- il est possible de cliquersur "ALERTES" en haut de l'application afin de faire apparaitre la liste des cages
qui ont été fermées par la présence d'une marmotte.
- il est possible cliquer sur cage pour aller à la page permettant de la mofifier
- Le bouton "+" en bas a droite permet d'ouvrir une page permettant d'ajouter une cage 
sur l'application

<br>
<div style="text-align:center"><img src ="pictures/maquette/Cages_principales_v1.png" />
<br>Page principale 
</div>

##### Cages en alertes

Cette page contient une liste similaire à la page principale sauf qu'elle contient
seulement les cages qui ont été fermées par le passage d'une marmotte. Ce sont 
donc les cages qui doivent vite être récupérées afin de préserver la santé de 
l'animal. En cliquant sur le bouton "Je suis enroute", le chercheur indique qu'il 
a bien reçu l'information et qu'il est en direction du lieu de la cage.

<br>
<div style="text-align:center"><img src ="pictures/maquette/Alertes_v1.png" />
<br>Cages en alerte 
</div>

##### Modification du nom ou de l'état d'une cage

Sur cette page, l'utilisateur peut changer le nom d'une cage et l'éteindre/allumer.
Si la cage est éteinte, l'application ne prendra pas en compte la réception d'un
signal de fermeture indiquant la prise d'une marmotte. L'utilisateur peut ensuite valider
ou annuler les modifications avec les boutons en bas de la page.

<br>
<div style="text-align:center"><img src ="pictures/maquette/modification_v1.png" />
<br>Modifier une cage 
</div>

##### Changement de l'état d'une cage

Cette page s'ouvre lorsque l'utilisateur éteint une cage. En effet, cette action 
ne doit pas être pris à la légére. Puisque si la cage est indiqué comme éteinte,
l'application ne recevra pas les alertes à la fermture d'une cage provoquée par 
une marmotte.

<br>
<div style="text-align:center"><img src ="pictures/maquette/ON_OFF_v1.png" />
<br>Changement de l'état d'une cage 
</div>

#### Version 2

Dans cette version, toutes les fonctionnalités sont regroupées sur une même page.

##### Page principale

Cette page présente une liste des cages. Chaque item de la liste correspond à une
cage qui a un nom et un état affiché en-dessous de son nom. Il est également possible 
d'éteindre/allumer une cage en changeant l'état du "switch button" à droite de chaque 
item. Un bouton "+" flottant en bas à droite de la page permet d'ajouter une cage 
à la liste.

<br>
<div style="text-align:center"><img src ="pictures/maquette/Cages_v2.png" />
<br>Page principale 
</div>

##### Cages en alerte

Cette page contient une liste similaire à la page principale sauf qu'elle contient
seulement les cages qui ont été fermées par le passage d'une marmotte. Son design
est le même que celui de la version1.

<br>
<div style="text-align:center"><img src ="pictures/maquette/Alertes_v1.png" />
<br>Cages en alerte 
</div>

##### Modification du nom d'une cage

Si l'utilisateur souhaite modifier le nom d'une cage, il lui suffit de cliquer 
sur le nom de cette dernière sur la page principale de l'application. Aprés le
clique, le clavier va s'afficher en bas de la page pour permettre à l'utilisateur 
de modifier le nom de la cage. A coté du nom, le "switch button" sera remplacé par
2 boutons pour valider ou annuler la modification.

<br>
<div style="text-align:center"><img src ="pictures/maquette/Modification_v2.png" />
<br>Modification du nom d'une cage 
</div>

##### Changement de l'état d'une cage

Cette page s'ouvre lorsque l'utilisateur éteint une cage. En effet, cette action 
ne doit pas être pris à la légére. Puisque si la cage est indiqué comme éteinte,
l'application ne recevra pas les alertes à la fermture d'une cage provoquée par 
une marmotte.

<br>
<div style="text-align:center"><img src ="pictures/maquette/ON_OFF_v2.png" />
<br>Changement de l'état d'une cage 
</div>

### Choix techniques

Nous avons pour objectif de rendre notre solution informatique simple à installer
et à mettre en route sur n'importe quel type de serveur. Pour cela, nous avons 
créé un conteneur **Docker** pour la partie back-end de l'application. 
<br>
Pour l'utiliser, il suffit de créer et d'installer l'image **Docker** du projet en utilisant le fichier
*Dockerfile* fournit dans le dépôt du [back-end](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/18-19/8/serveur-node-red-cage-marmotte).
De cette manière, il est possible d'exécuter le projet depuis un serveur local ou distant sur le cloud via, par exemple, l'utilisation 
d'une machine virtuelle.
<br>
**N.B.** : Une liste d'étapes complémentaires est fournit dans la documentation spécifique du back-end.

#### Partie Back-end

##### NodeRed
Notre projet consiste essentiellement à réceptionner et interpréter des messages envoyés via le réseau LoRa. Pour cela, 
nous nous sommes tournés vers la technologie **IoT** (Internet Of Things) *NodeRed*. *NodeRed* présente l'avantage
de proposer un mode de développement de type flow/pipeline simple à prendre en main et permettant un prototypage rapide grâce
aux différentes briques logicielles pré-intégrées et son tableau de bord intuitif. 
<br>
De plus, *NodeRed* est basé sur la technologie *NodeJS* se qui nous permet de rester dans l'écosystème JavaScript avec *Ionic*.
Cela facilite donc l'apprentissage des langages et permet de nous concentrer davantage sur la conception de l'application 
plutôt que sur des détails techniques liés à la méconnaissance des langages de développement.
<br>
Finalement, *NodeRed* est une technologie qui ne nous fait pas faire de compromis et qui nous fait gagner du temps 
grâce à ses nombreux composants disponibles développés par une large communauté de développeur et grâce à sa facilité d'utilisation.

#### Partie Front-end

##### Ionic  
Nous avons utilisé la technologie Ionic se basant sur le superset de JavaScript *TypeScript* et sur le framework *Angular* 
développé par Google pour le développement de l'application mobile. 
L'utilisation de Ionic s'explique par le fait que cette technologie permet un développement cross-plateform. 
De plus, le temps fixé pour la réalisation du projet étant relativement court, il nous aurait été compliqué 
de développer l'application nativement sur les deux plateformes que sont Androïd et IOS en utilisant ainsi 2 technologies différentes. 
Nous avons donc choisi de développer en Ionic qui, en plus de cela, est une technologie que nous avons appris en troisième année 
ce qui raccourcit le temps de prise en main par l'équipe de dévloppement. 

##### Firebase
Pour le stockage de nos données, nous avons choisi d'utiliser le service *Firebase* de Google. 
Ce dernier propose un service de stockage et d'accès vers une base de données en temps réel (gratuite jusqu'a 1Go de stockage et 100 connexions simultanées). 
Etant donnée le besoin de mise en place rapide de la base de données et de son intégration avec l'application mobile ainsi que
le peu de données que nous devons stocker pour le moment, l'utilisation de Firebase nous a semblé pertinent.
En effet, *Firebase* est un service utilisant du *NoSQL* (Not Only SQL) qui permet un développement très agile du fait de 
l'absence d'un modèle relationnel concret (contrairement à *MySQL* ou *SQLite*). Nous n'avons d'ailleurs, du fait de sa simplicité, par réellement
besoin d'un modèle relationnel pour notre projet ce qui nous a conforté dans le choix de cette technologie.
<br>
En addition à cela, il est peu probable que le service soit fermé par son gérant car de nombreuses applications utilisent 
ce service dont certaines avec 100 millions d'utilisateurs par mois (exemple : Shazam et le Figaro).
Néanmoins, il nous semble également primordial de faire de la redondance de données afin d'être paré à toutes éventualités. Nous n'avons juste
pas priorisé cet aspect durant le projet pour pouvoir nous concentrer un maximum sur la partie applicative, les fonctionnalités.

## Réalisation

### Sodaq Explorer
Le code final n'étant pas censé vraiment faire partie de notre projet (car réalisé par une équipe de l'IUT RT), 
nous n'avons développé qu'un code servant de plateforme de test nous permettant de vérifier les fonctionnalités développées 
sur notre serveur *NodeRed*, et sur notre application *Ionic*. 
<br>
Ainsi, le code écrit permet de simuler une cage, avec un ID changeable, 
et permet lors de l'appui sur un bouton d'envoyer un message en **LoRa** permettant de simuler la fermeture de la cage.  
Ce paquet est directement reçu par l'antenne LoRa la plus proche, et est disponible à la lecture sur la plateforme *LoRaServer*.  
<br>
<br>
Une des difficultés rencontrées à été la mise en place de l'environnement de développement pour le *Sodaq* 
(installation des packages requis pour cette carte spécifique, résolution des problèmes d'écriture sur la carte, etc). 
Une fois ces problèmes résolus, l'écriture du code C permettant de réaliser nos tests a été rapide et opérationnel rapidement, 
nous permettant de continuer sur la suite du projet : notre serveur et notre application.

### Serveur NodeRed
Pour récupérer les données reçues par la plateforme *LoRaServer* et les utiliser, nous avons choisi d'utiliser 
un serveur *NodeRed*. Ce serveur récupère donc via *MQTT* des trames LoRa en provenance des cages à marmotte et va récupérer l'information qu'elles contiennent, 
c'est-à-dire un `id` et un `état` de cage (ouvert ou fermé).  
<br>
Si une cage passe de l'état ouvert à fermé, il faut impérativement prévenir un des chercheurs. Pour cela, nous avons adopté 
deux moyens de transmettre l'information :  
- La mise à jour de la base de données utilisée par l'application, lui permettant ainsi de vérifier directement sur l'application l'état de la cage, son nom, sa date de fermeture.
  (nécessite une connexion internet 3G/4G)
- via SMS, un message est envoyé avec l'id de la cage et son état, ainsi que l'heure de fermeture.  

Ainsi, si le réseau internet est faible dans la zone où est présent le chercheur, il recevra quand même un SMS qui nécessite beaucoup moins de débit pour fonctionner.  
Il est très facile de rajouter des moyens de notification par la suite grâce à NodeRed, tel que des emails, d'autres numéro de téléphones.. etc.

### Développement de l'application mobile

Nous avons commencé le développment de l'application par l'initialisation d'une application *Ionic* blanche. 
Nous avons ensuite modifié celle-ci pour y créer le modèle de notre future application.
<br>
Plus précisément, nous avons ajouté les différentes pages nécessaires, les listes qui contiennent les cages et 
les champs de formulaire (ajouter et modifier une cage). Nous avons ensuite intégré l'application avec le service *Firebase* afin 
de pouvoir ajouter ou modifier des données depuis l'application. 
<br>
Pour finir, nous avons relié le serveur NodeRed et verifié que la modification de notre base de données par la fermeture d'une cage était bien apparente 
sur l'application en direct avec un service internet est disponible (Pour rappel : si internet n'est pas disponible, 
un SMS est envoyé pour alerter de la fermeture d'une cage).

### Résumé de la situation actuelle

Le schéma ci-dessous résume relativement bien l'avancement actuel du projet :
![schema_projet](pictures/schema.png)

## Évolutions potentielles

Le temps du projet étant relativement court, la version actuelle contient les fonctionnalités de base pour être utilisable le plus vite possible. 
Cependant, il est possible d'ajouter à l'application de nombreuses autres fonctionnalités : 
- Ajout de redondance dans les données pour être paré face à un éventuel problème du service Firebase.
- Ajouter de la sécurité sur les données ou créer des utilisateurs qui devront s'identifier pour utiliser l'application.
- Ajouter une carte intéractive pour indiquer la localisation d'une cage qui s'est fermée à cause de la présence d'une marmotte.
- Ajouter la possibilité de recevoir des notifications et pas seulement des SMS si le réseau de l'internet est disponible et qu'une cage s'est refermée.
- Pouvoir gérer le serveur plus facilement plus facilement que via NodeRed (changer le numéro de téléphone par exemple)
