# Application mobile Ionic/NodeRed pour Cages à marmotte connectées en LoRaWAN

## Objectif

__Contexte__: 
L’Internet des objets. L’Internet des objets (IoD, en anglais Internet
of things, IoT) représente l’extension d’Internet à des choses et à des lieux du
monde physique. L’objet connecté à Internet est identifié par le réseau de manière
unique comme l’est un ordinateur relié à Internet. L’objet connecté embarque 
l’intelligence nécessaire pour générer des données (paramètres de fonctionnement,
données d’usage, mesures physiques de son environnement, etc.), en capter de son
environnement et les transférer automatiquement sur le réseau. A l’autre bout,
une plateforme informatique centralise et traite les données collectées et gère
les services à valeur ajoutée créés de l’exploitation des historiques des 
mesures collectées au cours du temps. 
Les domaines d’application de l’Internet
des objets couvrent des pans entiers d’activités (e-santé, e-inclusion, sport
connecté, ville connectée, usine du futur, agriculture de précision, gestion des
risques naturelles et industrielles, smart office, smart building, logistique,
déplacement doux, etc.). L’internet des objets est considéré comme la troisième
évolution.

__Le projet__:
La Station alpine Joseph Fourier est une unité mixte de services de l'université
Grenoble Alpes et du CNRS. Elle déploie ses activités au col du Lautaret 
(Jardin botanique alpin, Chalet-laboratoire et Galerie de l'Alpe) et sur le 
campus de Grenoble (arboretum Robert Ruffier-Lanche et serres en cours de 
construction). 
Ses missions concernent le développement de plateformes de recherche, 
l’entretien de collections botaniques, la formation des étudiants et la 
vulgarisation scientifique auprès du grand public.
La Station Alpine Joseph Fourier du Col du Lautaret a déjà fait part de son 
intérêt pour plusieurs objets IoT non disponibles dans le commerce, 
comme une cage à marmotte connectée permettant au biologiste d’intervenir 
rapidement afin de faire des relevés biologiques et de relâcher l’animal 
qui meurt s’il reste trop longtemps dans la cage.
L'objet de surveillance des cages est en cours de réalisation par un groupe 
d'étudiants de l'IUT1 Grenoble (R&T et GMP). Le projet consistera à développer 
l'application de monitoring d'une ensemble de cages. Cette application est 
destiné aux biologistes qui sont munis de smartphones (Android ou iOs).
Ce projet est réalisé avec un groupe d'étudiants de l'IUT1 Grenoble (R&T et GMP).

## Contraintes technologiques

* L'application doit être développée avec Ionic/Cordova
* L'application doit pouvoir fonctionner en l'absence de couverture 3G/4G ou SMS

## Compte rendu de la réunion du 4 février

Le systéme de capture des marmottes comprend un grand nombre de cage. Chaque
cage est identifiée par un nom et est située à un emplacement statique et 
prédéfini. Il est possible de reconnaitre une cage en considérant l'identifiant 
d'un des capteurs à l'origine du message LoRa envoyé au moment de la capture. Au
moment de la capture, la cage envoit des messages en continue jusqu'a ce qu'un 
membre de l'équipe de recherche confirme la réception du message. Cette 
confirmation devra se faire depuis l'application mobile après la reception d'un 
message d'avertissement. On peut imaginer utiliser une carte interactive sur 
laquelle serait disposée la totalité des cages avec un voyant lumineux indiquant 
la localisation de la cage dans laquelle une marmotte vient d'être capturée. La 
nuit, les chercheurs ferment les cages manuellement afin de ne pas capturer 
d'animal sans personnel sur le terrain. Notre application devra donc recevoir un
message et prendre en compte cette fermeture en la mettant dans un état
d'hibernation. Il faudra de plus offrir la fonctionnalité permettant d'ajouter 
une nouvelle cage au dispositif ou d'en supprimer une. 

## Journal de bord 

### Semaine 1 (29 janvier - 3 février)

Nous avons consacré cette semaine à l'initialistaion du projet sur GitLab 
comprenant dans un premier temps le fichier README.md et les fichiers de base
lors de la création d'une application Ionic.
De plus, nous avons suivis le cours donné lundi 29 janvier après-midi sur la 
technologie LoRaWAN afin de pouvoir commencer le projet dans les meilleurs 
conditions.
L'objectif à court terme est maintenant d'identifier les exigences fonctionnelles 
et non fonctionnelles du bioligiste.

### Semaine 2 (4 février - 10 février)

Nous avons consacré notre séance du lundi après-midi à une réunion avec 
Mr Delbart et Mlle Araujo afin d'avoir un première aperçu des besoins de 
l'application. A suivis une bréve explication des outils technologiques qui
vont être utilisé lors du développement.

### Semaine 3 (11 février - 18 février)

Nous avons consacré l'ensemble de la semaine à la mise en place d'un serveur à 
l'aide de Microsoft Azure. Nous n'avons pas réussi à terminer l'installation
complétement. Il faudra donc continuer la mise en place la semaine suivante.
Dans le même temps, nous avons terminé de parametrer notre carte sodaq Explorer
afin de pouvoir controler l'envoi des paquets avec un bouton, et controler le contenu des paquets.
Nous avons donc un moyen stable de tester la reception/lecture des paquets sur notre serveur.

### Semaine 4 (18 février - 25 février)

Après avoir passé trop de temps à chercher à mettre en place une solution sur 
le cloud, nous avons réagi en décidant de nous focaliser davantage sur le projet 
lui même. Pour cela, nous avons mis en place un environnement de développement 
pour le serveur *NodeRed* via la récupération et l'adaptation du **DockerFile** 
fourni dans le tuto *NodeRed* situé sur le site [CampusIOT](https://github.com/CampusIoT/tutorial/tree/master/nodered).
<br>
<br>
Nous avons donc commencé la rédaction de la démarche à suivre pour générer l'image
*Docker* puis l'exécuter sur une machine. Fournir notre projet dans une image
*Docker* permettra une intégration rapide sur n'importe quelle machine locale, 
ou distante via le cloud.

### Semaine 5 et 6 (25 février - 11 mars)

Cette semaine, nous avons commencé à développer une ébauche du projet sur *NodeRed*
en créant un *Flow* se connectant à [LoraServer](https://lora.campusiot.imag.fr/#/organizations/4/applications).
et récupérant les informations envoyées via le *Siconia* fourni par *M. Donsez*.
Ces informations correspondent en l'occurrence à une température en degrés Celsius.
Notre premier programme *NodeRed* a donc consisté à recevoir le message émis
par le *Siconia* et envoyé sur **LoraServer** pour ensuite récupérer la température
et l'envoyer par mail/sms à quelqu'un.
<br>
![flow_basique](pictures/s6_flow.png)
<br><br>
Cela ressemble grossièrement à l'idée de notre projet. En effet, les deux capteurs
présents sur chaque cage à marmotte vont nous envoyer des données du type :
```json
{
    "cage_id" : 43,
    "status" : 1,
    "time" : "15:41:32"
}
```
Nous devrons donc préparer une réponse adéquate, via une fonction *NodeRed*, 
qui pourrait ressembler à cela dans le cadre d'un sms :
<br>
`[Alert] A marmot entered in the cage n°43 at 15h 41m 32s, you have 30 min !`
On devra faire en sorte que le serveur génère une notification push pour
l'application mobile ainsi qu'un sms au cas où le réseau mobile soit insuffisant
pour disposer d'internet (**réseau 2G**). 
<br><br>
Pour continuer nos expérimentations, on pourrait modifier le code présent sur
le *Siconia* pour faire en sorte qu'il envoi un message plus ressemblant à 
ce que nous allons vraiment devoir traiter en réalité.
<br>
Il nous faudra ensuite commencer à développer la partie *Ionic* mobile.
En parallèle du travail de cette semaine sur *NodeRed*, nous avons aussi 
commencé la réalisation du diagramme de cas d'utilisation et de la maquette de
notre application mobile.
<br>
![flow_basique](pictures/Conception/case_diagram.jpg)
<br><br>

### Semaines suivantes

Les dernières semaines du projet ont été consacrées au développement de 
l'application mobile avec la technologie IONIC. Nous avons commencé par la mise
en place du template de notre future application puis nous avons passé plusieurs 
heures pour relier le service Firebase, Node RED et notre application.
A suivis une phase de test pour corriger certains bug ou améliorer certaines 
fonctionnalités.

